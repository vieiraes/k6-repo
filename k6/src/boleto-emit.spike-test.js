import http from 'k6/http';
import { sleep, check } from 'k6';
import { url, payload, params } from './post-boleto-emit-config.js';
import { uuidv4 } from 'https://jslib.k6.io/k6-utils/1.1.0/index.js';

export const options = {
    stages: [
        { duration: '10s', target: 10 },
        { duration: '3m', target: 200 },
        { duration: '3m', target: 200 },
        { duration: '2m', target: 0 },
    ],
    thresholds: {
        http_req_duration: ['p(99)<1000'], // 99% of requests must complete below 1s
    },
};

export default () => {
    params.headers['x-idempotence-key'] = uuidv4();
    const res = http.post(url, payload, params);
    check(res, { 'status is 201': (r) => r.status === 201 });

    if (res.status < 299) {
        console.warn('/////////////////////////// Responde Status: ' + res.status + ' Boleto id: '+ res.json().id);
    } else {
        console.error('=========================== Error: ' + res.status);
    }
    
    sleep(1);
};



