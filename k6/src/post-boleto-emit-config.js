//const taxId = '29003621000100';
const taxId = '47899939000101';

//export const url = 'http://172.17.0.1:3010/emission/beneficiary/19983487000106/boleto';
export const url = 'https://testnet.btcore.app/boletos/emission/beneficiary/'+taxId+'/boleto';

export const payload = JSON.stringify({
    amount: '1',
    billing: {
        externalId: 1
    },
    destination: '2bd26675-418a-4b2f-9336-bceae1f89fb2',
    extra: {
        expiresAt: '2022-04-30',
    },
});

export const params = {
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer TOKEN'
    },
};