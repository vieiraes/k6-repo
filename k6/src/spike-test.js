import http from 'k6/http';
import { sleep, group } from 'k6';
import { uuidv4 } from 'https://jslib.k6.io/k6-utils/1.1.0/index.js';
import { url, payload, params } from './post-boleto-emit-config.js';

export const options = {
    insecureSkipTLSVerify: true,
    noConnectionReuse: false,
    stages: [
        { duration: '10s', target: 10 },
        { duration: '5m', target: 1000 },
        { duration: '5m', target: 1000 },
        { duration: '4m', target: 0 },
    ]
};

export default () => {

    group('Emitir boleto com beneficário existente', () => {
        params.headers['x-idempotence-key'] = uuidv4();
        const res = http.post(url, payload, params);
        console.log('Status: ' + res.status + ' Boleto id: '+ res.json().id);
        sleep(1);
    });
};
