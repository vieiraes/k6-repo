import http from 'k6/http';
import { sleep, check } from 'k6';
import { url, payload, params } from './post-boleto-emit-config-matera.js';

export const options = {
    stages: [
        { duration: '20s', target: 5 },
        { duration: '40s', target: 10 },
        { duration: '60s', target: 100 },
        { duration: '40s', target: 10 },
        { duration: '20s', target: 1 },
        { duration: '10s', target: 0 },
    ],
    thresholds: {
        http_req_duration: ['p(99)<1000'], // 99% of requests must complete below 1s
    },
};

export default () => {
    const res = http.post(url, payload, params);
    check(res, { 'status is 200': (r) => r.status === 200 });

    if ( (res.status < 299) && (res.body) ) {
        console.warn('/////////////////////////// Responde Status: ' + res.status + ' Boleto idMatera: '+ res.json().data.idTitulo);
    } else {
        console.error('=========================== Error: ' + res.status);
    }
    
    sleep(1);
};



