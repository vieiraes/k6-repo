export const url = 'https://iapi-banking.hml.amedigital.com/sdcobr/api/v2/titulos';

export const payload = JSON.stringify({
    idBeneficiario: 1,
    seuNumero: Date.now().toString().slice(-10),
    carteira: 1,
    tipoTitulo: 1,
    valorTitulo: 1,
    dataDocumento: "2022-03-10",
    dataVencimento: "2022-04-30",
    pagador: {
        nome: "MATERA SYSTEMS",
        tipo: "J",
        cnpjCpf: 57040040000184,
        cep: 4552000,
        endereco: "RUA DO ROCIO",
        cidade: "SAO PAULO",
        uf: "SP",
        bairro: "JD IMPERIAL",
        email: "teste@matera.com"
    }
});

export const params = {
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer TOKEN'
    },
};