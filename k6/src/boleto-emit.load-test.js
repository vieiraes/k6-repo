import http from 'k6/http';
import { sleep, check } from 'k6';
import { url, payload, params } from './post-boleto-emit-config.js';
import { uuidv4 } from 'https://jslib.k6.io/k6-utils/1.1.0/index.js';

export const options = {
  stages: [
    { duration: '5m', target: 100 }, // simulate ramp-up of traffic from 1 to 100 users over 5 minutes.
    { duration: '10m', target: 100 }, // stay at 100 users for 10 minutes
    { duration: '5m', target: 0 }, // ramp-down to 0 users
  ],
  thresholds: {
    http_req_duration: ['p(99)<1000'], // 99% of requests must complete below 1s
  },
};

export default () => {
  params.headers['x-idempotence-key'] = uuidv4();
  const res = http.post(url, payload, params);
  check(res, { 'status is 201': (r) => r.status === 201 });
  console.log('Status: ' + res.status + ' Boleto id: '+ res.json().id);
  sleep(1);
};
